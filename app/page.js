import Image from 'next/image';
import A from "/public/a.jpg";
import B from "/public/b.jpg";
import C from "/public/c.jpg";
import AA from "/public/aa.jpg";
import BB from "/public/bb.jpg";
import CC from "/public/cc.jpg";
import React from "react";
import Econ from "../components/Econ.js"

export default async function Page(){
    const css = { width: '80%', height: 'auto' }


    return(

            <div className={"bg-slate-100 w-screen h-screen"}>
                <div className={"w-0 h-0 shadow-red-500 shadow-orange-500 shadow-amber-500 shadow-yellow-500 shadow-orange-500 shadow-amber-500 shadow-yellow-500 shadow-lime-500 shadow-green-500 shadow-emerald-500 shadow-teal-500 shadow-cyan-500 shadow-sky-500 shadow-blue-500 shadow-violet-500 fushsia-500 pink-500 rose-500"}></div>

            <div className={"text-sans"}>

                <Econ></Econ>
                <Econ></Econ>

                <h1 className={"text-7xl mx-auto text-center underline decoration-dashed decoration-indigo-500 animate-bounce pt-10"}>samuel-gram</h1>

                <Image className={"mx-auto my-5 rounded-lg hover:shadow-blue-500 shadow-2xl shadow-purple-500"} alt={""} src={CC} sizes="70vw" priority={true} style={css}>
                </Image>
                <Image className={"mx-auto my-5 rounded-lg hover:shadow-blue-500 shadow-2xl shadow-purple-500"} alt={""} src={B}  sizes="70vw" priority={true} style={css}></Image>
                <Image className={"mx-auto my-5 rounded-lg hover:shadow-blue-500 shadow-2xl shadow-purple-500"}  alt={""} src={C}  sizes="70vw" priority={true} style={css}></Image>
                <Image className={"mx-auto my-5 rounded-lg hover:shadow-blue-500 shadow-2xl shadow-purple-500"}  alt={""} src={AA}  sizes="70vw" priority={true} style={css}></Image>
                <Image className={"mx-auto my-5 rounded-lg hover:shadow-blue-500 shadow-2xl shadow-purple-500"}  alt={""} src={BB}  sizes="70vw" priority={true} style={css}></Image>
                <Image className={"mx-auto my-5 rounded-lg hover:shadow-blue-500 shadow-2xl shadow-purple-500"}  alt={""} src={A}  sizes="70vw" priority={true} style={css}></Image>

            </div>
            </div>
    )
}
