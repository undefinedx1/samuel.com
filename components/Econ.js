"use client";
import Confetti from "react-confetti";
import React from "react";


export default function Page(){

    const [width, setWidth] = React.useState(0)
    const [height, setHeight] = React.useState(0)

    React.useEffect(()=> {
        if (typeof window != "undefined") {

            setWidth(window.innerWidth)
            setHeight(window.innerHeight * 4 )

        }
    })
    return(
    <Confetti
        width={width}
        height={height}
    />)
}