export function objectToRows(data){

    let rows = []
    // Traverse the Object full of Properties
    for (let docID in data){
        // For each property
        let row = {id: docID}  // create a new row representing the property
        let propertyInfoCollection = data[docID]

        for(let documentName in propertyInfoCollection) { // traverse the info collection
            row[documentName] = propertyInfoCollection[documentName]
        }
        rows.push(row)
    }

    return rows

}

export function objectToColumns(data) {
    let columns = [{"field": "id", headerName: "id", width:250, type:"string"}]
    for (let docID in data) {     // traverse all property keys in data
        let info = data[docID]
        for (let documentID in info) {   // traverse all documents in info
            // for each thing in the info collection
            let found = false // in the thing has a column
            for(let elem in columns) { // for each document in info, check to see if it has a column
                if (columns[elem].field === documentID) {
                    found = true
                }
            }
            if (!found) {
                if (documentID === "value") {
                    columns.push({field: documentID, headerName: documentID, width: 125, editable: true, type: "number"})
                } else if (documentID === "autopay"){
                    columns.push({field: documentID, headerName: documentID, width: 125, editable: true, type: "boolean"})
                } else if (documentID === "timing"){
                columns.push({field: documentID, headerName: documentID, width: 125, editable: true, type: "number"})
                }
                else {
                    columns.push({field: documentID, headerName: documentID, width: 125, editable: true})
                }
            }
        }

    }




    return columns;
}

export function asc(a,b){
    return b.field - a.field
}