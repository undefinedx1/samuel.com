"use client";
import {DataGridPremium, GridToolbar, GridApi, useGridApiRef} from "@mui/x-data-grid-premium";
import {Box} from "@mui/material";
import React from "react";
import {onSnapshot, doc, updateDoc, setDoc, getFirestore, collection} from "firebase/firestore";
import {db, units} from "../public/firebase";
import {objectToColumns, objectToRows} from "./dataTransformations";


export default function DataGridClient(props){

    const apiRef = useGridApiRef()


    const [rows, setRows] = React.useState(props.rows)
    const [columns, setColumns] = React.useState(props.columns)
    const [gridState, setGridState] = React.useState(props.initalGridState)

    async function setGridStateCustom(state){
        setGridState(state)
        await setDoc(doc(db, "client", "cyberneticstream.com"), {
            data: JSON.stringify(state)
        });
    }

    React.useEffect(()=> {

        console.log(props.initalGridState)
        console.log("OKAy")
        console.log(apiRef.current.exportState())
        const unsubscribe = onSnapshot(units, (querySnapshot) => {
            const info = {};
            querySnapshot.forEach((doc) => {
                info[doc.id] = doc.data()
            });
            setRows(objectToRows((info)))
        });
        return () => {unsubscribe()}
    }, [])

    function getDetailPanelContent(){
        return(<h1>d</h1>)
    }

    async function processRowUpdate(newCellState, oldCellState){
        return new Promise(async function(resolve, reject) {
            for(let key in newCellState){
                if (newCellState[key] !== oldCellState[key]) {
                    const docRef = doc(db, "units/"+newCellState.id);
                    let update = { }
                    update[key] = newCellState[key]
                    await updateDoc(docRef, update).then((x) => resolve(newCellState))
                }
            }
            resolve(oldCellState)
        })
    }

    return(
            <Box sx={{height:"100vh", width:"100%"}}>
                <DataGridPremium
                    onColumnVisibilityModelChange={()=>setGridStateCustom(apiRef.current.exportState())}
                    onAggregationModelChange={()=>setGridStateCustom(apiRef.current.exportState())}
                    onCellClick={()=>setGridStateCustom(apiRef.current.exportState())}
                    onCellDoubleClick={()=>setGridStateCustom(apiRef.current.exportState())}
                    onCellEditCommit={()=>setGridStateCustom(apiRef.current.exportState())}
                    onCellEditStart={()=>setGridStateCustom(apiRef.current.exportState())}
                    onCellEditStop={()=>setGridStateCustom(apiRef.current.exportState())}
                    onCellFocusIn={()=>setGridStateCustom(apiRef.current.exportState())}
                    onCellFocusOut={()=>setGridStateCustom(apiRef.current.exportState())}
                    onCellKeyDown={()=>setGridStateCustom(apiRef.current.exportState())}
                    onCellKeyUp={()=>setGridStateCustom(apiRef.current.exportState())}
                    onColumnHeaderClick={()=>setGridStateCustom(apiRef.current.exportState())}
                    onColumnHeaderDoubleClick={()=>setGridStateCustom(apiRef.current.exportState())}
                    onColumnResize={()=>setGridStateCustom(apiRef.current.exportState())}
                    onFilterModelChange={()=>setGridStateCustom(apiRef.current.exportState())}
                    onMenuClose={()=>setGridStateCustom(apiRef.current.exportState())}
                    onMenuOpen={()=>setGridStateCustom(apiRef.current.exportState())}
                    onResize={()=>setGridStateCustom(apiRef.current.exportState())}
                    onPreferencePanelClose={()=>setGridStateCustom(apiRef.current.exportState())}
                    onPreferencePanelOpen={()=>setGridStateCustom(apiRef.current.exportState())}
                    onRowOrderChange={()=>setGridStateCustom(apiRef.current.exportState())}


                    initialState={gridState}
                    apiRef = {apiRef}
                    columns={columns}
                    rows={rows}
                    processRowUpdate={processRowUpdate}
                    components={{Toolbar: GridToolbar}}
                    checkboxSelection
                    density ="compact"
                    rowReordering={true}
                    experimentalFeatures={{aggregation: true, newEditingApi: true}}
                    getDetailPanelContent={getDetailPanelContent}
                    getDetailPanelHeight={() => "auto"}
                />
            </Box>
            )
}